Repeated Square Root 

```math
\sqrt{\sqrt{\sqrt{\sqrt{\sqrt{x}}}}} = x^{1/{2^5}}  
```

What happens when one repeatedly square root of a positive real number?  
* = [take $`2^n`$'th roots](https://math.stackexchange.com/questions/3283/why-do-i-always-get-1-when-i-keep-hitting-the-square-root-button-on-my-calculat)
* = taking [repeated square roots](http://edspi31415.blogspot.com/2015/08/repeated-presses-of-square-root-button.html) 

```math
x^{1/2^n}\to 1  
\\
as \space n \to \infty 
```
It goes to to 1. It is [the limit of sequence](https://en.wikipedia.org/wiki/Limit_of_a_function#Sequential_limits).  


$` \lim_{n \to \infty} x^{1/2^n} = 1`$


It can be also treated as a discrete dynamical system:  

$` x_{n+1} = \sqrt{x_n} `$

where taking each square root is called iteration.

One can analyze the distance $`d`$ to the limit point $`x_{\infty} `$

$` d_{n} = x_n - x_{\infty} `$

where 


$` x_{\infty} = 1 `$

Example :  backward iteration on the external ray 0 for c = 0 $` z_{n-1} = \sqrt{z_n} `$  
Then limit point is called the landing point of the ray. 

See also : [backward iteration on the external ray in the Siegel disc case](https://gitlab.com/adammajewski/ray-backward-iteration) 





# precision
* double: [double.c](double.c) and it's output in [double.txt](double.txt)
* long double: [long.c](long.c) and it's output in [long.txt](long.txt)
* arbitrary: [arb.c](arb.c) and it's output in [arb.txt](arb.txt)


Using double precision and linear scale of Y axis: distance is tending to zero very fast ( like exponential function)   
![double_linear.png](double_linear.png)  


[Linear scale on both axes](https://en.wikipedia.org/wiki/Cartesian_coordinate_system) is good for first view, but it is hard to check what is happening near 0. Let's switch to [the logarithmic scale](https://en.wikipedia.org/wiki/Logarithmic_scale) 

  
Using double precision and log scale of Y axis :  enough precision only for 50 iterations    
  
![double_log.png](double_log.png)  


Using long double precision and log scale of Y axis : : enough precision only for 60 iterations   

![long_log.png](long_log.png)  


Using arbitrary precision ( [arb library](http://arblib.org/) see [arb.c file](arb.c)) and log scale of Y axis : all ( here 500) iterations have enough precision ( because precision was adjusted at each step )  

![arb.png](arb.png)  





# How to create image from data in Gnuplot?

First collect the data in the text file:

```bash
./a.out > data.txt
```

then open gnuplot:

```bash
gnuplot
```

and draw them to the png file:


```gnuplot
# gnuplot
set terminal png
set output "double_linear.png"
set title "repeated square root of 2 : distance to 1.0"
set xlabel "iteration"
set ylabel "distance"
unset key
# linear scale
plot "double.txt" with lines
# logscale
set logscale y
set output "t1l.png"
plot "t1.txt" 
plot "t1.txt" with lines
# read the data in the scientific format
set format y "%e";

```

# License

See [LICENSE](LICENSE)

# git

```git
git init
git remote add origin git@gitlab.com:adammajewski/repeated-square-root-
git add .
git commit -m "descr"
git push -u origin master
```


local git repo: 
```bash
~/c/arb/arb_sqrt/distance$ 
```


## markdown format
[GitLab](https://gitlab.com/) uses:
* the Redcarpet Ruby library for [Markdown processing](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md)
* KaTeX to render [math written with the LaTeX syntax](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md), but [only subset](https://khan.github.io/KaTeX/function-support.html)



