/* 

   original file : 
   This file is public domain. Author: Fredrik Johansson. 
   https://raw.githubusercontent.com/fredrik-johansson/arb/master/examples/logistic.c


  


   

 
  gcc arb.c -larb -lflint -lgmp  -L/usr/local/lib/ -lmpfr  -Wall
  
  ./a.out >arb.txt

   -----------------
   relative determinate error in the square root of Q is one half the relative determinate error in Q


   IEEE-754 standard guarantees me that the result will be as close to the 'real result' as possible, i.e. error will be less than or equal to 1/2 unit-in-the-last-place. In particular,
   http://stackoverflow.com/questions/22259537/guaranteed-precision-of-sqrt-function-in-c-c
 
   =====================
   decimal digits required = ceil(-log10(sqrt(x+1)-sqrt(x)))
 
 
   http://stackoverflow.com/questions/30029410/sqrt-and-decimals
 
 
   ============
   https://groups.google.com/forum/#!topic/flint-devel/yrOCnfi6DuI
 
   I thought that the number will be decreasing but will be greater then 1.0 ( not equal to 1.0) 

   Correct.
 
   and I will display such number ( all digits ) 

   Note that the ball radius in most of your results is larger than the first nonzero digit after the 1.

   For example, if I have the number 1.000123456.... with a ball radius of 1e-2, I will only see 1.00+/-1e-2. If I have a ball radius of 1e-5, I'll see 1.00012+/-1e-5.

   In your calculation, you kept the precision at 64 bits for too long. Then you started doubling the precision unnecessarily. Try increasing the precision by 1 bit on every iteration, right from the start, instead of doubling it after the digits of interest are already gone.
   Bill Hart 
 
====================




gnuplot
set terminal png
set output "arb.png"
set format y "%e";
set logscale y
set title "distance from point to the limiting point after n repeted square roots of 2"


# arb

[m±r]≡[m−r,m+r] where the midpoint m and the radius r are (extended) real numbers

remove radius from arb.txt manually using column mode editing 
https://askubuntu.com/questions/1126156/how-to-select-a-column-or-block-of-text-in-gedit-3-28-1


or replace +/- with \t 
and use third columnas an yerrorbar ( but it is so small the will not be visible )

*/

#include "arb.h"


int main()
{
  slong prec;  //  the number of bits needed to represent the absolute value of the mantissa of the midpoint of x, i.e. the minimum precision sufficient to represent x exactly.
  slong i =0;
  // arb_t : An arb_t represents a ball over the real numbers, that is, an interval [m±r]≡[m−r,m+r] where the midpoint m and the radius r are (extended) real numbers
  arb_t z; //  arbitrary-precision floating-point numbers with ball arithmethic
  arb_t zl; //  limit value
  arb_t d;
  
  
  
  
    

  // init   
  arb_init(z);
  arb_init(zl);
  arb_init(d);
  
          
  
  // set
  arb_set_ui(z,  2); // z = 2.0
  arb_set_ui(zl, 1); // z = 2.0
  arb_set_ui(d,  1); // z = 2.0
  prec = 64; // start with double precision 
   
    
  flint_printf("i\t d\n"); // print header 
        
        
  while (1)
    {
      
      
      // print results ( i and d )   in one row    
      flint_printf("%3wd \t", i);
      arb_printd(d, 20); // http://arblib.org/using.html
      flint_printf("\n");   
         
          
      // computations
      arb_sqrt(z, z, prec); // iteration z = sqrt(z)
      arb_sub(d, z, zl, prec); // distance d = z - z_l
      prec += 1;  // increasing the precision by 1 bit on every iteration
      
      
      // loop index
      if (i>500) break; 
      i+=1;
             
        
    }
   
  // print info 
  flint_printf("Computed with: \narb-%s\n  Flint-%s\n MPFR-%s \n GMP-%s \n", arb_version, FLINT_VERSION ,mpfr_version, gmp_version ); //


  // clear
  arb_clear(z);
  arb_clear(zl);
  arb_clear(d);
  flint_cleanup();
  
  
  return 0;
}

