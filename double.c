/*
  gcc t1.c -Wall -lm
  
  https://pl.wikibooks.org/wiki/Programowanie_w_systemie_UNIX/ARB#arb_sqrt
  
  
  the error will less than 1/2 ULP (unit in the last place) or basically as close as possible to the actual answer.
  https://stackoverflow.com/questions/20137105/c-sqrt-function-precision-for-full-squares
  
  
  
 
  
*/  
#include <math.h> 
#include <stdio.h>

int main ()
{
  
  double x0 = 2.0;
  double xl = 1.0;
  double x ;
  int i;
  double d; 
 
 x = x0;
 for (i = 0; i<100; i++) 
     {
	d = (x-xl);
	printf (" i = %d d = %.16f \n", i,d);      
	x=sqrt(x);
	 
       
    }

return 0;
}


