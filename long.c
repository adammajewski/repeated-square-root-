/*
  gcc t1.c -Wall -lm
  ./a.out
  a.out > l.txt
  
  https://pl.wikibooks.org/wiki/Programowanie_w_systemie_UNIX/ARB#arb_sqrt
  
 

# gnuplot
set terminal png
set output "double_linear.png"
set title "repeated square root of 2 : distance to 1.0"
set xlabel "iteration"
set ylabel "distance"
unset key
plot "double.txt" with lines

set logscale y
set output "t1l.png"
plot "t1.txt" 
plot "t1.txt" with lines
gnuplot> set output "t1l.png"
gnuplot> plot "t1.txt" with lines
gnuplot> set title "repeated square root of 2 using double: distance to 1.0"
gnuplot> set output "t1l.png"
gnuplot> plot "t1.txt" with lines

  
*/  
#include <math.h> 
#include <stdio.h>

int main ()
{
  
  long double x0 = 2.0L;
  long double xl = 1.0L;
  long double x ;
  int i;
  long double d; 
 
 x = x0;
 for (i = 0; i<100; i++) 
     {
	d = (x-xl);
	printf (" %d \t %.19Lf \n", i,d);      
	x=sqrtl(x);
	 
       
    }

return 0;
}


